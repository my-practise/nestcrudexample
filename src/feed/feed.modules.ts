import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { FeedController } from "./feed.controller";
import { FeedService } from "./feed.service";
import { FeedPostEntity } from "./models/post.entity";

@Module({
    imports:[
        TypeOrmModule.forFeature([FeedPostEntity])

    ],
    providers: [FeedService],
    controllers:[FeedController],
})
export class FeedModule{}