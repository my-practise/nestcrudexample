import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common";
import { Observable } from "rxjs";
import { DeleteResult, UpdateResult } from "typeorm";
import { FeedService } from "./feed.service";
import { FeedPost } from "./models/post.interface";

@Controller('feed')
export class FeedController{
    constructor(private feedservice: FeedService){}
    @Post()
    create(@Body() post: FeedPost): Observable<FeedPost>{
        return this.feedservice.createPost(post);
    }

    @Get()
    findAll(): Observable<FeedPost []>{
        return this.feedservice.findAllPost();
    }

    @Put(':id')
    update(@Param('id')id:number,@Body() post: FeedPost ):Observable<UpdateResult>{
        return this.feedservice.update(id,post);
    }

    @Delete(':id')
    delete(@Param('id')id:number):Observable<DeleteResult>{
        return this.feedservice.deletePost(id);

    }
}